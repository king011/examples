# socks5

socks5 服務器 示例代碼

```sh
# -l 指定 代理服務 監聽 端口
# -u 指定 開啓 udp 轉發
socks5 -l ":1080" -u

# -user 指定 用戶配置檔案 使用 用戶名/密碼 驗證
# -l 指定 代理服務 監聽 端口
# -u 指定 開啓 udp 轉發
socks5 -user user.json -l ":1080" -u
```