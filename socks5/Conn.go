package main

import (
	"context"
	"io"
	"net"
	"time"
)

// Conn 包裝 net.Conn 提供一些 常用的 額外方法
type Conn struct {
	net.Conn
	chanRead    chan error
	chanWrite   chan error
	read, write int
}

// NewConn 包裝 net.Conn
func NewConn(c net.Conn) (conn *Conn) {
	conn = &Conn{
		chanRead:  make(chan error),
		chanWrite: make(chan error),
	}
	conn.Conn = c
	return
}

// WriteTimeout 寫入 數據
//
// 同一時間 只能有一個 goroutine ReadTimeout
func (c *Conn) WriteTimeout(b []byte, timeout time.Duration) (e error) {
	length := len(b)
	if length == 0 {
		return
	}
	if timeout < 1 {
		e = context.DeadlineExceeded
		return
	}

	go func() {
		if _, e := c.Write(b); e == nil {
			c.chanWrite <- nil
		} else {
			c.chanWrite <- e
		}
	}()

	// 超時 定時器
	t := time.NewTimer(timeout)
	// 等待 完成
	select {
	case <-t.C:
		e = context.DeadlineExceeded
	case e = <-c.chanWrite:
		// 停止 定時器
		if !t.Stop() {
			<-t.C
		}
	}
	return
}

// ReadTimeout 讀取 指定 長度數據
//
// 同一時間 只能有一個 goroutine ReadTimeout
func (c *Conn) ReadTimeout(b []byte, timeout time.Duration) (e error) {
	length := len(b)
	if length == 0 {
		return
	}
	if timeout < 1 {
		e = context.DeadlineExceeded
		return
	}
	// 超時 定時器
	t := time.NewTimer(timeout)

	// 讀取 數據
	r := io.LimitReader(c, int64(length))
	go func() {
		pos := 0
		for pos != length {
			n, err := r.Read(b[pos:])
			if err != nil {
				// 通知 出錯
				c.chanRead <- err
				return
			}
			pos += n
		}
		// 通知 完成
		c.chanRead <- nil
	}()

	// 等待 完成
	select {
	case <-t.C:
		e = context.DeadlineExceeded
	case e = <-c.chanRead:
		// 停止 定時器
		if !t.Stop() {
			<-t.C
		}
	}
	return
}
