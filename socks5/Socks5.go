package main

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"time"
)

// Socks5 代理服務器實現
type Socks5 struct {
	l     net.Listener
	udp   bool
	users map[string]string
}

// NewSocks5 創建一個 socks5 服務器
func NewSocks5(users map[string]string, laddr string, udp bool) (s *Socks5, e error) {
	var l net.Listener
	l, e = net.Listen("tcp", laddr)
	if e != nil {
		return
	}
	s = &Socks5{
		users: users,
		l:     l,
		udp:   udp,
	}
	return
}

// Run 運行 服務
func (s *Socks5) Run() {
	l := s.l
	for {
		c, e := l.Accept()
		if e != nil {
			log.Println(e)
			continue
		}

		// 建立 連接
		go s.initAccept(NewConn(c))
	}
}
func (s *Socks5) initAccept(c *Conn) {
	defer c.Close()

	// 讀取 客戶端 支持的 認證方式
	timeout := time.Second * 5

	// 只要注意下 實現 緩衝區 最大時 就是 接收 socks5請求 時 的 請求/響應 包
	// VER	CMD	RSV	ATYP	DST.ADDR	DST.PORT
	// 1    1   1   1       1+255       2
	b := make([]byte, 262)
	e := c.ReadTimeout(b[:2], timeout)
	if e != nil {
		log.Println(e)
		return
	}
	if b[0] != 0x5 {
		log.Printf("VER must be 0x05, not support 0x%02x\n", b[0])
		return
	}
	if b[1] < 1 || b[1] > 255 {
		log.Printf("NMETHODS must range [1,255], not support %v\n", b[1])
		return
	}
	n := b[1]
	e = c.ReadTimeout(b[:n], timeout)
	if e != nil {
		log.Println(e)
		return
	}

	// 查找 支持的 認證
	for _, v := range b[:n] {
		switch v {
		case 0x00: // 無需認證
			// 當 沒有配置用戶時 才 允許 無需認證
			if s.users == nil {
				// 通知 無需驗證
				b[0] = 0x05
				b[1] = 0x00
				e = c.WriteTimeout(b[:2], timeout)
				if e != nil {
					log.Println(e)
					return
				}
				// 等待 客戶端 發送 代理請求
				s.waitRequest(c, b, timeout)
				return
			}
		case 0x02: // 用戶名/密碼 認證
			// 當 配置了用戶時 才 允許 用戶名/密碼 認證
			if s.users != nil {
				// 進行 用戶名/密碼 認證
				var yes bool
				yes, e = s.verifyUsernameAndPassword(c, b, timeout)
				if e != nil {
					log.Println(e)
					return
				} else if yes {
					// 等待 客戶端 發送 代理請求
					s.waitRequest(c, b, timeout)
				}
				return
			}
		}
	}

	// 協商 認證 失敗
	b[0] = 0x05
	b[1] = 0xFF
	c.WriteTimeout(b[:2], timeout)
}
func (s *Socks5) verifyUsernameAndPassword(c *Conn, b []byte, timeout time.Duration) (yes bool, e error) {
	// 通知使用 用戶名/密碼 進行 認證
	b[0] = 0x05
	b[1] = 0x02
	e = c.WriteTimeout(b[:2], timeout)
	if e != nil {
		return
	}
	// 驗證 鑑定協定版本
	e = c.ReadTimeout(b[:2], timeout)
	if e != nil {
		return
	}
	if b[0] != 0x1 {
		e = fmt.Errorf("not support Verify Username And Password Version [0x%02x]", b[0])
		return
	}
	// 獲取 用戶名
	var user string
	n := b[1]
	if b[1] == 0 {
		e = errors.New("ULEN must range [1,255")
		return
	}
	e = c.ReadTimeout(b[:n+1], timeout)
	if e != nil {
		return
	}
	user = string(b[:n])
	n = b[n]

	// 獲取密碼
	var password string
	if n == 0 {
		e = errors.New("PLEN must range [1,255")
	} else {
		e = c.ReadTimeout(b[:n], timeout)
		if e != nil {
			return
		}
		password = string(b[:n])
	}

	// 驗證 用戶名 密碼
	b[0] = 0x01
	if v, ok := s.users[user]; ok && v == password {
		yes = true
		b[1] = 0x00
		e = c.WriteTimeout(b[:2], timeout)
	} else {
		b[1] = 0x01
		e = c.WriteTimeout(b[:2], time.Second)
	}
	return
}
func (s *Socks5) waitRequest(c *Conn, b []byte, timeout time.Duration) {
	// 等待 客戶端 發送 請求
	e := c.ReadTimeout(b[:4], timeout)
	if e != nil {
		log.Println(e)
		return
	}
	if b[0] != 0x5 {
		log.Printf("VER must be 0x05, not support 0x%02x\n", b[0])
		return
	}
	switch b[1] {
	case 0x01:
		// 執行 CONNECT
		s.connect(c, b, timeout)
		return
	case 0x02:
		log.Println("not support BIND")
	case 0x03:
		log.Println("not support UDP")
	default:
		log.Printf("not support unknow CMD [0x%02x]\n", b[1])
	}

	// 通知 不支持 指令
	b[0] = 0x05
	b[1] = 0x07 // 不支持的 命令
	b[2] = 0
	b[3] = 0x01
	c.WriteTimeout(b[:4+4+2], time.Second)
}
func (s *Socks5) connect(c *Conn, b []byte, timeout time.Duration) {
	// 返回 請求目標
	response, addr, port, e := s.getDst(c, b, timeout)
	if e != nil {
		log.Println(e)
		return
	}
	// 連接 目標
	addr += ":" + strconv.Itoa(int(port))
	var sc net.Conn
	sc, e = net.Dial("tcp", addr)
	if e != nil {
		// 通知 connect 失敗
		log.Println(e)
		return
	}

	// 通知 客戶端 連接成功
	response[1] = 0x00
	e = c.WriteTimeout(response, timeout)
	if e != nil {
		log.Println(e)
	}

	// 轉發 tcp 數據
	go func() {
		for {
			if _, e := io.Copy(c, sc); e != nil {
				break
			}
		}
		sc.Close()
		c.Close()
	}()
	for {
		if _, e := io.Copy(sc, c); e != nil {
			break
		}
	}
	sc.Close()
	c.Close()
}
func (s *Socks5) getDst(c *Conn, b []byte, timeout time.Duration) (response []byte, addr string, port uint16, e error) {
	switch b[3] {
	case 0x01: //ipv4
		e = c.ReadTimeout(b[4:4+4+2], timeout)
		if e != nil {
			return
		}
		addr = net.IP(b[4 : 4+4]).String()
		port = binary.BigEndian.Uint16(b[4+4:])
		response = b[:4+4+2]
	case 0x03: //domain
		e = c.ReadTimeout(b[4:4+1], timeout)
		if e != nil {
			return
		}
		n := int(b[4])
		e = c.ReadTimeout(b[4+1:4+1+n+2], timeout)
		addr = string(b[4+1 : 4+1+n])
		port = binary.BigEndian.Uint16(b[4+1+n:])
		response = b[:4+1+n+2]
	case 0x04: //ipv6
		e = c.ReadTimeout(b[4:4+16+2], timeout)
		if e != nil {
			return
		}
		addr = net.IP(b[4 : 4+16]).String()
		port = binary.BigEndian.Uint16(b[4+16:])
		response = b[:4+16+2]
	default:
		e = fmt.Errorf("not support ATYP [0x%02x]", b[3])
	}
	return
}
