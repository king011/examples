package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	var laddr string
	var help, udp bool
	var user string
	flag.StringVar(&laddr, "l", "localhost:1080", "socks5 server listen address")
	flag.StringVar(&user, "user", "", "user define")
	flag.BoolVar(&udp, "u", false, "if true support udp")
	flag.BoolVar(&help, "h", false, "display help")
	flag.Parse()
	if help {
		flag.PrintDefaults()
		return
	}
	// 讀取 用戶配置
	var users map[string]string
	if user != "" {
		b, e := ioutil.ReadFile(user)
		if e != nil {
			log.Fatalln(e)
		}
		var arrs []struct {
			Name     string
			Password string
		}
		e = json.Unmarshal(b, &arrs)
		if e != nil {
			log.Fatalln(e)
		}
		if len(arrs) != 0 {
			users = make(map[string]string)
			for _, node := range arrs {
				users[node.Name] = node.Password
			}
		}

	}

	// 創建 服務器
	s, e := NewSocks5(users, laddr, udp)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("socks5 woat at", laddr)
	// 運行 服務
	s.Run()
}
